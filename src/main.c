#include <pebble.h>

enum dict_keys {
	COMMAND_ID,
	BAR_COUNT,
	BAR_INDEX,
	BAR_ID,
	BAR_NAME,
	BAR_DISTANCE_I,
	BAR_DISTANCE_D,
	SONG_NAME,
	SONG_ARTIST,
	SONG_ALBUM
};

enum commands {
	CONFIGURE,
	SEARCH,
	RECENT_RESULTS,
	RECENT_RESULT,
	NEARBY_RESULTS,
	NEARBY_RESULT,
	PLAYING
};

struct bar {
	uint32_t id;
	char name[30];
	uint16_t distance_i;
	uint8_t distance_d;
};

struct bars {
	uint8_t length;
	struct bar bars[5];
};

static GFont gothic_18_bold;
static GFont gothic_24;
static GFont gothic_24_bold;
static GFont gothic_28_bold;

static GBitmap *refresh_icon;

static Window *barWindow;
static Window *playingWindow;

static TextLayer *barWindow_searchingText;
static TextLayer *barWindow_configureText;
static MenuLayer *barWindow_barMenu;

static ActionBarLayer *playingWindow_actionBar;
static TextLayer *playingWindow_loadingText;
static TextLayer *playingWindow_playingSong;
static TextLayer *playingWindow_playingArtist;
static TextLayer *playingWindow_playingAlbum;

static struct bars recentBars = { .length = 0 };
static struct bars nearbyBars = { .length = 0 };

static uint32_t playing_bar_id;

void barMenu_draw_header(GContext *ctx, const Layer *cell_layer, uint16_t section_index, void *callback_context){
	if( (section_index && nearbyBars.length) || (!section_index && !recentBars.length && nearbyBars.length) ){
		menu_cell_basic_header_draw(ctx, cell_layer, "Nearby");
	} else if( recentBars.length ){
		menu_cell_basic_header_draw(ctx, cell_layer, "Recent");
	}
}

void barMenu_draw_row(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context){
	char *distance = "65,536.99 mi";
	struct bars theseBars;
	struct bar thisBar;
	int result;

	if( (cell_index->section && nearbyBars.length) || (!cell_index->section && !recentBars.length && nearbyBars.length) ){
		theseBars = nearbyBars;
	} else if( recentBars.length ){
		theseBars = recentBars;
	} else {
		return;
	}
	thisBar = theseBars.bars[cell_index->row];
	result = snprintf(distance, 13, "%u.%.2hu mi", thisBar.distance_i, thisBar.distance_d);
	if( result < 1 ){
		APP_LOG(APP_LOG_LEVEL_ERROR, "snprintf() failed!");
	}
	menu_cell_basic_draw(ctx, cell_layer, thisBar.name, distance, NULL);
}

int16_t barMenu_header_height(struct MenuLayer *menu_layer, uint16_t section_index, void *callback_context){
	return MENU_CELL_BASIC_HEADER_HEIGHT;
}

uint16_t barMenu_num_rows(struct MenuLayer *menu_layer, uint16_t section_index, void *callback_context){
	if( (section_index && nearbyBars.length) || (!section_index && !recentBars.length && nearbyBars.length) ){
		return nearbyBars.length;
	} else if( recentBars.length ){
		return recentBars.length;
	}
	return 0;
}

uint16_t barMenu_num_sections(struct MenuLayer *menu_layer, void *callback_context){
	if( recentBars.length && nearbyBars.length ){
		return 2;
	} else if( recentBars.length || nearbyBars.length ){
		return 1;
	}
	return 0;
}

void barMenu_select_click(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context){
	struct bars theseBars;
	struct bar thisBar;

	if( cell_index->section || (!recentBars.length && nearbyBars.length) ){
		theseBars = nearbyBars;
	} else if( recentBars.length ){
		theseBars = recentBars;
	} else {
		return;
	}
	thisBar = theseBars.bars[cell_index->row];
	playing_bar_id = thisBar.id;
	DictionaryIterator *dict;
	AppMessageResult result = app_message_outbox_begin(&dict);
	if( result == APP_MSG_OK ){
		DictionaryResult dict_result = dict_write_uint8(dict, COMMAND_ID, PLAYING);
		if( dict_result ){
			APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_uint8 failed with reason %d", dict_result);
			return;
		} else {
			if( (dict_result = dict_write_uint32(dict, BAR_ID, playing_bar_id)) ){
				APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_uint32 failed with reason %d", dict_result);
				return;
			}
			if( dict_write_end(dict) ){
				result = app_message_outbox_send();
				if( result != APP_MSG_OK ){
					APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_send failed with reason %d", result);
					return;
				}
			} else {
				APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_end failed");
				return;
			}
		}
	} else {
		APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_begin failed with reason %d", result);
		return;
	}
	window_stack_push(playingWindow, true);
}

void barWindow_load(Window *window){
	barWindow_searchingText = text_layer_create(GRect(0, 62, 144, 28));
	// layer_set_hidden((Layer *)barWindow_searchingText, true);
	text_layer_set_text(barWindow_searchingText, "Searching...");
	text_layer_set_text_alignment(barWindow_searchingText, GTextAlignmentCenter);
	text_layer_set_font(barWindow_searchingText, gothic_24_bold);
	layer_add_child(window_get_root_layer(barWindow), (Layer *)barWindow_searchingText);

	barWindow_configureText = text_layer_create(GRect(0, 50, 144, 52));
	layer_set_hidden((Layer *)barWindow_configureText, true);
	text_layer_set_text(barWindow_configureText, "Please configure app on phone");
	text_layer_set_text_alignment(barWindow_configureText, GTextAlignmentCenter);
	text_layer_set_overflow_mode(barWindow_configureText, GTextOverflowModeWordWrap);
	text_layer_set_font(barWindow_configureText, gothic_24_bold);
	layer_add_child(window_get_root_layer(barWindow), (Layer *)barWindow_configureText);

	barWindow_barMenu = menu_layer_create(GRect(0, 0, 144, 152));
	layer_set_hidden((Layer *)barWindow_barMenu, true);
	menu_layer_set_callbacks(barWindow_barMenu, NULL, (MenuLayerCallbacks){
		.draw_header = barMenu_draw_header,
		.draw_row = barMenu_draw_row,
		.get_header_height = barMenu_header_height,
		.get_num_rows = barMenu_num_rows,
		.get_num_sections = barMenu_num_sections,
		.select_click = barMenu_select_click,
		.get_separator_height = NULL,
	});
	menu_layer_set_click_config_onto_window(barWindow_barMenu, barWindow);
	layer_add_child(window_get_root_layer(barWindow), (Layer *)barWindow_barMenu);
}

void barWindow_appear(Window *window){
	if( ! recentBars.length && ! nearbyBars.length ){
		DictionaryIterator *dict;
		AppMessageResult result = app_message_outbox_begin(&dict);
		if( result == APP_MSG_OK ){
			DictionaryResult dict_result = dict_write_uint8(dict, COMMAND_ID, SEARCH);
			if( dict_result ){
				APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_uint8 failed with reason %d", dict_result);
			} else {
				if( dict_write_end(dict) ){
					result = app_message_outbox_send();
					if( result != APP_MSG_OK ){
						APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_send failed with reason %d", result);
					}
				} else {
					APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_end failed");
				}
			}
		} else {
			APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_begin failed with reason %d", result);
		}
	}
	layer_mark_dirty(window_get_root_layer(barWindow));
}

void barWindow_unload(Window *window){
	menu_layer_destroy(barWindow_barMenu);
	text_layer_destroy(barWindow_searchingText);
	text_layer_destroy(barWindow_configureText);
}

void playingWindow_SelectClickHandler(ClickRecognizerRef recognizer, void *context){
	layer_set_hidden((Layer *)playingWindow_loadingText, false);
	layer_set_hidden((Layer *)playingWindow_playingSong, true);
	layer_set_hidden((Layer *)playingWindow_playingArtist, true);
	layer_set_hidden((Layer *)playingWindow_playingAlbum, true);
	DictionaryIterator *dict;
	AppMessageResult result = app_message_outbox_begin(&dict);
	if( result == APP_MSG_OK ){
		DictionaryResult dict_result = dict_write_uint8(dict, COMMAND_ID, PLAYING);
		if( dict_result ){
			APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_uint8 failed with reason %d", dict_result);
			return;
		} else {
			if( (dict_result = dict_write_uint32(dict, BAR_ID, playing_bar_id)) ){
				APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_uint32 failed with reason %d", dict_result);
				return;
			}
			if( dict_write_end(dict) ){
				result = app_message_outbox_send();
				if( result != APP_MSG_OK ){
					APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_send failed with reason %d", result);
					return;
				}
			} else {
				APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_end failed");
				return;
			}
		}
	} else {
		APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_begin failed with reason %d", result);
		return;
	}
}

void playingWindow_actionBar_ClickConfigProvider(void *context){
	window_single_click_subscribe(BUTTON_ID_SELECT, playingWindow_SelectClickHandler);
}

void playingWindow_load(Window *window){
	playingWindow_actionBar = action_bar_layer_create();
	action_bar_layer_add_to_window(playingWindow_actionBar, playingWindow);
	action_bar_layer_set_icon(playingWindow_actionBar, BUTTON_ID_SELECT, refresh_icon);
	action_bar_layer_set_click_config_provider(playingWindow_actionBar, playingWindow_actionBar_ClickConfigProvider);

	playingWindow_loadingText = text_layer_create(GRect(0, 62, 144 - ACTION_BAR_WIDTH, 28));
	// layer_set_hidden((Layer *)playingWindow_loadingText, true);
	text_layer_set_text(playingWindow_loadingText, "Loading...");
	text_layer_set_text_alignment(playingWindow_loadingText, GTextAlignmentCenter);
	text_layer_set_font(playingWindow_loadingText, gothic_24_bold);
	layer_add_child(window_get_root_layer(playingWindow), (Layer *)playingWindow_loadingText);

	playingWindow_playingSong = text_layer_create(GRect(4, 48, 140 - ACTION_BAR_WIDTH, 84));
	layer_set_hidden((Layer *)playingWindow_playingSong, true);
	text_layer_set_text_alignment(playingWindow_playingSong, GTextAlignmentLeft);
	text_layer_set_overflow_mode(playingWindow_playingSong, GTextOverflowModeTrailingEllipsis);
	text_layer_set_font(playingWindow_playingSong, gothic_28_bold);
	layer_set_clips((Layer *)playingWindow_playingSong, false);
	layer_add_child(window_get_root_layer(playingWindow), (Layer *)playingWindow_playingSong);

	playingWindow_playingArtist = text_layer_create(GRect(4, 0, 140 - ACTION_BAR_WIDTH, 56));
	layer_set_hidden((Layer *)playingWindow_playingArtist, true);
	text_layer_set_text_alignment(playingWindow_playingArtist, GTextAlignmentLeft);
	text_layer_set_overflow_mode(playingWindow_playingArtist, GTextOverflowModeTrailingEllipsis);
	text_layer_set_font(playingWindow_playingArtist, gothic_24);
	layer_set_clips((Layer *)playingWindow_playingArtist, false);
	layer_add_child(window_get_root_layer(playingWindow), (Layer *)playingWindow_playingArtist);

	playingWindow_playingAlbum = text_layer_create(GRect(4, 128, 140 - ACTION_BAR_WIDTH, 22));
	layer_set_hidden((Layer *)playingWindow_playingAlbum, true);
	text_layer_set_text_alignment(playingWindow_playingAlbum, GTextAlignmentLeft);
	text_layer_set_overflow_mode(playingWindow_playingAlbum, GTextOverflowModeTrailingEllipsis);
	text_layer_set_font(playingWindow_playingAlbum, gothic_18_bold);
	layer_set_clips((Layer *)playingWindow_playingAlbum, false);
	layer_add_child(window_get_root_layer(playingWindow), (Layer *)playingWindow_playingAlbum);
}

void playingWindow_appear(Window *window){
	layer_mark_dirty(window_get_root_layer(playingWindow));
}

void playingWindow_unload(Window *window){
	action_bar_layer_remove_from_window(playingWindow_actionBar);
	action_bar_layer_destroy(playingWindow_actionBar);
	text_layer_destroy(playingWindow_loadingText);
	text_layer_destroy(playingWindow_playingSong);
	text_layer_destroy(playingWindow_playingArtist);
	text_layer_destroy(playingWindow_playingAlbum);
}

void app_message_inbox_dropped(AppMessageResult reason, void *context){
	APP_LOG(APP_LOG_LEVEL_ERROR, "Incoming message dropped!");
}

void app_message_inbox_received(DictionaryIterator *iterator, void *context){
	Tuple *command;
	command = dict_find(iterator, COMMAND_ID);
	if( command ){
		Tuple *bar_count = dict_find(iterator, BAR_COUNT);
		Tuple *bar_index = dict_find(iterator, BAR_INDEX);
		Tuple *bar_id = dict_find(iterator, BAR_ID);
		Tuple *bar_name = dict_find(iterator, BAR_NAME);
		Tuple *bar_distance_i = dict_find(iterator, BAR_DISTANCE_I);
		Tuple *bar_distance_d = dict_find(iterator, BAR_DISTANCE_D);
		Tuple *song_name = dict_find(iterator, SONG_NAME);
		Tuple *song_artist = dict_find(iterator, SONG_ARTIST);
		Tuple *song_album = dict_find(iterator, SONG_ALBUM);
		switch( command->value->int32 ){
			case CONFIGURE:
				layer_set_hidden((Layer *)barWindow_searchingText, true);
				layer_set_hidden((Layer *)barWindow_barMenu, true);
				layer_set_hidden((Layer *)barWindow_configureText, false);
				break;
			case RECENT_RESULTS:
				recentBars.length = bar_count->value->int32;
				layer_set_hidden((Layer *)barWindow_configureText, true);
				layer_set_hidden((Layer *)barWindow_searchingText, true);
				layer_set_hidden((Layer *)barWindow_barMenu, false);
				break;
			case NEARBY_RESULTS:
				nearbyBars.length = bar_count->value->int32;
				layer_set_hidden((Layer *)barWindow_configureText, true);
				layer_set_hidden((Layer *)barWindow_searchingText, true);
				layer_set_hidden((Layer *)barWindow_barMenu, false);
				break;
			case RECENT_RESULT:
				recentBars.bars[bar_index->value->int32].id = bar_id->value->int32;
				recentBars.bars[bar_index->value->int32].distance_i = bar_distance_i->value->int32;
				recentBars.bars[bar_index->value->int32].distance_d = bar_distance_d->value->int32;
				memset(recentBars.bars[bar_index->value->int32].name, 0, 30);
				strncpy(recentBars.bars[bar_index->value->int32].name, bar_name->value->cstring, strlen(bar_name->value->cstring) > 29 ? 29 : strlen(bar_name->value->cstring));
				menu_layer_reload_data(barWindow_barMenu);
				break;
			case NEARBY_RESULT:
				nearbyBars.bars[bar_index->value->int32].id = bar_id->value->int32;
				nearbyBars.bars[bar_index->value->int32].distance_i = bar_distance_i->value->int32;
				nearbyBars.bars[bar_index->value->int32].distance_d = bar_distance_d->value->int32;
				memset(nearbyBars.bars[bar_index->value->int32].name, 0, 30);
				strncpy(nearbyBars.bars[bar_index->value->int32].name, bar_name->value->cstring, strlen(bar_name->value->cstring) > 29 ? 29 : strlen(bar_name->value->cstring));
				menu_layer_reload_data(barWindow_barMenu);
				break;
			case PLAYING:
				APP_LOG(APP_LOG_LEVEL_DEBUG, "Setting playing song text to %s", song_name->value->cstring);
				text_layer_set_text(playingWindow_playingSong, song_name->value->cstring);
				text_layer_set_text(playingWindow_playingArtist, song_artist->value->cstring);
				text_layer_set_text(playingWindow_playingAlbum, song_album->value->cstring);
				layer_set_hidden((Layer *)playingWindow_loadingText, true);
				layer_set_hidden((Layer *)playingWindow_playingSong, false);
				layer_set_hidden((Layer *)playingWindow_playingArtist, false);
				layer_set_hidden((Layer *)playingWindow_playingAlbum, false);
				break;
			default:
				break;
		}
	} else {
		APP_LOG(APP_LOG_LEVEL_ERROR, "Malformed message!");
	}
}

void app_message_outbox_failed(DictionaryIterator *iterator, AppMessageResult reason, void *context){
	Tuple *command;
	APP_LOG(APP_LOG_LEVEL_ERROR, "Outgoing message failed with reason %d", reason);
	command = dict_find(iterator, COMMAND_ID);
	if( command && command->type == TUPLE_UINT && command->length == 1 ){
		uint8_t action = command->value->uint8;
		uint32_t bar_id = 0;
		if( action == PLAYING ){
			Tuple *bar = dict_find(iterator, BAR_ID);
			if( bar && bar->type == TUPLE_UINT && bar->length == 4 ){
				bar_id = bar->value->uint32;
			} else {
				APP_LOG(APP_LOG_LEVEL_ERROR, "Malformed message!");
			}
		}
		DictionaryIterator *dict;
		AppMessageResult result = app_message_outbox_begin(&dict);
		if( result == APP_MSG_OK ){
			DictionaryResult dict_result = dict_write_uint8(dict, COMMAND_ID, action);
			if( dict_result ){
				APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_uint8 failed with reason %d", dict_result);
			} else {
				if( action == PLAYING ){
					if( (dict_result = dict_write_uint32(dict, BAR_ID, bar_id)) ){
						APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_uint32 failed with reason %d", dict_result);
						return;
					}
				}
				if( dict_write_end(dict) ){
					result = app_message_outbox_send();
					if( result != APP_MSG_OK ){
						APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_send failed with reason %d", result);
					}
				} else {
					APP_LOG(APP_LOG_LEVEL_ERROR, "dict_write_end failed");
				}
			}
		} else {
			APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_outbox_begin failed with reason %d", result);
		}
	} else {
		APP_LOG(APP_LOG_LEVEL_ERROR, "Malformed message!");
	}
}

void app_message_outbox_sent(DictionaryIterator *iterator, void *context){
}

void init(void){
	AppMessageResult result;
	
	gothic_18_bold = fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD);
	gothic_24 = fonts_get_system_font(FONT_KEY_GOTHIC_24);
	gothic_24_bold = fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD);
	gothic_28_bold = fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD);

	refresh_icon = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_ACTION_ICON_REFRESH);

	app_message_register_inbox_dropped(app_message_inbox_dropped);
	app_message_register_inbox_received(app_message_inbox_received);
	app_message_register_outbox_failed(app_message_outbox_failed);
	app_message_register_outbox_sent(app_message_outbox_sent);
	result = app_message_open(app_message_inbox_size_maximum(), app_message_outbox_size_maximum());
	if( result ){
		APP_LOG(APP_LOG_LEVEL_ERROR, "app_message_open result was %d", result);
	}

	barWindow = window_create();
	playingWindow = window_create();
	window_set_window_handlers(barWindow, (WindowHandlers){
		.load = barWindow_load,
		.appear = barWindow_appear,
		.unload = barWindow_unload
	});
	window_set_window_handlers(playingWindow, (WindowHandlers){
		.load = playingWindow_load,
		.appear = playingWindow_appear,
		.unload = playingWindow_unload
	});
	window_stack_push(barWindow, true);
}

void deinit(void){
	gbitmap_destroy(refresh_icon);
	window_destroy(playingWindow);
	window_destroy(barWindow);
}

int main(void){
	init();
	app_event_loop();
	deinit();
	return 0;
}

var dict_keys = {
	COMMAND_ID: '0',
	BAR_COUNT: '1',
	BAR_INDEX: '2',
	BAR_ID: '3',
	BAR_NAME: '4',
	BAR_DISTANCE_I: '5',
	BAR_DISTANCE_D: '6',
	SONG_NAME: '7',
	SONG_ARTIST: '8',
	SONG_ALBUM: '9'
};
var commands = {
	CONFIGURE: 0,
	SEARCH: 1,
	RECENT_RESULTS: 2,
	RECENT_RESULT: 3,
	NEARBY_RESULTS: 4,
	NEARBY_RESULT: 5,
	PLAYING: 6
};
var authentication = "";
var playerId = 0;
var authing = false;
var messageQueue = [];

function auth(afterAuth){
	authing = true;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("POST", "https://mobile.amientertainment.net/mobileserver/mobile/user/login", true);
	xmlhttp.onload = function(e){
		if( xmlhttp.readyState == 4 ){
			if( xmlhttp.status == 200 ){
				var response = JSON.parse(xmlhttp.responseText);
				if( response.result === 0 ){
					authentication = response.authentication;
					playerId = response.playerId;
					authing = false;
					afterAuth();
				} else if( response.result == 8 ){
					console.log("Bad password!");
				} else if( response.result == 18 ){
					console.log("Email not found!");
				}
			} else {
				console.log("Login status code error: " + xmlhttp.status);
			}
		}
	};
	xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xmlhttp.send(JSON.stringify({id: localStorage.getItem('email'), password: localStorage.getItem('password')}));
}

function queuePush(message){
	messageQueue[messageQueue.length] = message;
	if( messageQueue.length == 1 ){
		Pebble.sendAppMessage(message, ackHandler, nackHandler);
	}
}

function isAnonymous(){
	return localStorage.getItem('anonymous') != "false";
}

function apiCall(endpoint, onload, data){
	data = data || null;
	var xmlhttp = new XMLHttpRequest();
	if( isAnonymous() ){
		if( data ){
			xmlhttp.open("POST", endpoint, true);
		} else {
			xmlhttp.open("GET", endpoint, true);
		}
		xmlhttp.onload = function(e){ onload(xmlhttp, e); };
		if( data ){
			xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xmlhttp.send(JSON.stringify(data));
		} else {
			xmlhttp.send();
		}
	} else {
		xmlhttp.open("POST", endpoint, true);
		xmlhttp.onload = function(e){ onload(xmlhttp, e); };
		data.authentication = authentication;
		data.playerId = playerId;
		xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xmlhttp.send(JSON.stringify(data));
	}
}

function ackHandler(e){
	messageQueue.shift();
	if( messageQueue.length ){
		Pebble.sendAppMessage(messageQueue[0], ackHandler, nackHandler);
	}
}

function nackHandler(e){
	console.log('Unable to deliver message with transactionId=' + e.payload.transactionId);
	Pebble.sendAppMessage(messageQueue[0], ackHandler, nackHandler);
}

function handleBars(response, types, type){
	if( response.locations.length > 5 ){
		response.locations.length = 5;
	}
	var message = {};
	message[dict_keys.COMMAND_ID] = types;
	message[dict_keys.BAR_COUNT] = response.locations.length;
	queuePush(message);
	for( var i = 0; i < response.locations.length; i++ ){
		var bar = response.locations[i];
		message = {};
		message[dict_keys.COMMAND_ID] = type;
		message[dict_keys.BAR_INDEX] = i;
		message[dict_keys.BAR_ID] = bar.id;
		message[dict_keys.BAR_NAME] = bar.name;
		message[dict_keys.BAR_DISTANCE_I] = Math.floor(bar.distance);
		message[dict_keys.BAR_DISTANCE_D] = Math.floor((bar.distance - Math.floor(bar.distance)) * 100);
		queuePush(message);
	}
}

function handleRecent(xmlhttp, e){
	if( xmlhttp.readyState == 4 ){
		if( xmlhttp.status == 200 ){
			var response = JSON.parse(xmlhttp.responseText);
			if( response.result == 0 ){
				handleBars(response, commands.RECENT_RESULTS, commands.RECENT_RESULT);
			}
		}
	}
}

function handleSearch(xmlhttp, e){
	if( xmlhttp.readyState == 4 ){
		if( xmlhttp.status == 200 ){
			var response = JSON.parse(xmlhttp.responseText);
			if( response.result === 0 ){
				handleBars(response, commands.NEARBY_RESULTS, commands.NEARBY_RESULT);
			}
		}
	}
}

function getSong(url){
	var data = null;
	if( isAnonymous() ){
		url = url + '?includeSong=true';
	} else {
		data = {includeSong: true};
	}
	apiCall(url, function(xmlhttp, e){handlePlaying(xmlhttp, e, url);}, data);
}

function handlePlaying(xmlhttp, e, url){
	if( xmlhttp.readyState == 4 ){
		if( xmlhttp.status == 200 ){
			var response = JSON.parse(xmlhttp.responseText);
			if( response.result === 0 ){
				var nowPlaying = response.devices[0].nowPlayingSongInfo;
				var message = {};
				message[dict_keys.COMMAND_ID] = commands.PLAYING;
				if( nowPlaying ){
					message[dict_keys.SONG_NAME] = nowPlaying.songTitle;
					message[dict_keys.SONG_ARTIST] = nowPlaying.artistName;
					message[dict_keys.SONG_ALBUM] = nowPlaying.albumName;
				} else {
					message[dict_keys.SONG_NAME] = "None";
					message[dict_keys.SONG_ARTIST] = "None";
					message[dict_keys.SONG_ALBUM] = "None";
				}
				queuePush(message);
			} else if( response.result == 9 ){
				auth(function(){getSong(url);});
			}
		}
	}
}

function locationSuccess(pos){
	var latitude = pos.coords.latitude;
	var longitude = pos.coords.longitude;
	if( !isAnonymous() ){
		apiCall("https://mobile.amientertainment.net/mobileserver/mobile/location/recent", handleRecent, {
			geocode: {
				lat: latitude,
				lng: longitude,
				provider: "network"
			}
		});
	}
	apiCall("https://mobile.amientertainment.net/mobileserver/mobile/location/search", handleSearch, {
		"attributes": [],
		"deviceTypes": [
			1
		],
		"geocode": {
			"lat": latitude,
			"lng": longitude,
			"provider": "network"
		},
		"page": 1,
		"resultsPerPage": 20
	});
}

function locationError(err){
	console.log('ERROR(' + err.code + '): ' + err.message);
}

Pebble.addEventListener('ready', function(e){
	if( localStorage.getItem('anonymous') === null || ( !isAnonymous() && ( localStorage.getItem('email') === '' || localStorage.getItem('password') === '' ) ) ){
		var message = {};
		message[dict_keys.COMMAND_ID] = commands.CONFIGURE;
		queuePush(message);
		return;
	}
	if( !isAnonymous() ){
		auth(function(){navigator.geolocation.getCurrentPosition(locationSuccess, locationError, {maximumAge: 600000, timeout: 10000});});
	} else {
		navigator.geolocation.getCurrentPosition(locationSuccess, locationError, {maximumAge: 600000, timeout: 10000});
	}
});

Pebble.addEventListener('appmessage', function(e){
	switch( e.payload[dict_keys.COMMAND_ID] ){
		case commands.SEARCH:
			if( localStorage.getItem('anonymous') === null || ( !isAnonymous() && ( localStorage.getItem('email') === '' || localStorage.getItem('password') === '' ) ) ){
				var message = {};
				message[dict_keys.COMMAND_ID] = commands.CONFIGURE;
				queuePush(message);
				return;
			} else {
				if( !isAnonymous() ){
					if( authentication === "" && !authing ){
						auth(function(){navigator.geolocation.getCurrentPosition(locationSuccess, locationError, {maximumAge: 600000, timeout: 10000});});
					}
				} else {
					navigator.geolocation.getCurrentPosition(locationSuccess, locationError, {maximumAge: 600000, timeout: 10000});
				}
			}
			break;
		case commands.PLAYING:
			var url = 'https://mobile.amientertainment.net/mobileserver/mobile/location/id/' + e.payload[dict_keys.BAR_ID];
			getSong(url);
			break;
	}
});

Pebble.addEventListener('showConfiguration', function(e){
	Pebble.openURL('http://dwfreed.bitbucket.org/barlink/config.html#' + encodeURIComponent(JSON.stringify(localStorage)));
});

Pebble.addEventListener('webviewclosed', function(e){
	if( e.response !== undefined && e.response !== '' && e.response !== 'CANCELLED' ){
		var settings = JSON.parse(decodeURIComponent(e.response));
		for( var setting in settings ){
			if( settings.hasOwnProperty(setting) ){
				localStorage.setItem(setting, settings[setting]);
			}
		}
		if( !isAnonymous() ){
			auth(function(){navigator.geolocation.getCurrentPosition(locationSuccess, locationError, {maximumAge: 600000, timeout: 10000});});
		} else {
			navigator.geolocation.getCurrentPosition(locationSuccess, locationError, {maximumAge: 600000, timeout: 10000});
		}
	}
});
